@extends('layouts.app')

  

@section('content')

<div class="row">

    <div class="col-lg-12 margin-tb">

        <div class="pull-left">

            <h2>Add New Product</h2>

        </div>

        <div class="pull-right">

            <a class="btn btn-primary" href="{{ route('products.index') }}"> Back</a>

        </div>

    </div>

</div>

   

@if ($errors->any())

    <div class="alert alert-danger">

        <strong>Whoops!</strong> Dữ liêu bạn nhập vào không hợp lê:<br><br>

        <ul>

            @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

            @endforeach

        </ul>

    </div>

@endif

   

<form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">

    @csrf

  

     <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Name:</strong>

                <input type="text" name="name"  class="form-control"  class="form-control" placeholder="Name">
                <!-- <input type="text" name="name" pattern="[A-Za-z]" class="form-control" maxlength="150" minlength="50" class="form-control" placeholder="Name"> -->
            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>Detail:</strong>

                <textarea class="form-control" style="height:150px" class="form-control"  name="detail" placeholder="Detail"></textarea>
                <!-- <textarea class="form-control" style="height:150px" pattern="[A-Za-z]" class="form-control" maxlength="150" minlength="50" name="detail" placeholder="Detail"></textarea> -->
            </div>

        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">

            <div class="form-group">

                <strong>image:</strong>

                <input type="file" name="file"  class="form-control-file"  >

            </div>

        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 text-center">

                <button type="submit" class="btn btn-primary">Submit</button>

        </div>

    </div>

   

</form>

@endsection
<!-- Query.validator.addMethod("notNumber", function(value, element, param) {
                       var reg = /[0-9]/;
                       if(reg.test(value)){
                             return false;
                       }else{
                               return true;
                       }
                }, "Number is not permitted"); -->