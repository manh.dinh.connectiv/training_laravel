<?php

namespace App\Http\Controllers;

use App\Http\Requests\productValidate;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
class ProductController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $products = Product::latest()->paginate(5);
        //dd($products);
        return view('products.index',compact('products'));

            //->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('products.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(productValidate $request)
    {
        //$validated = $request->validated();
        //$this->validateRequest($request->all());
        
        $product = new Product();
   
        $product->name = $request->name;
        $product->detail = $request->detail;
        $file = $request->file;
        $fileName = $file->getClientOriginalName();

        $file->storeAs('public', $fileName);
     
        //$file->move('uploads',$fileName);
        $product->image = $fileName;

        $product->save();
        //Product::create($request->all());

        
        return redirect()->route('products.index')->with('success','Product created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        
        Event::fire('posts.view', $product);
        return view('products.show',compact('product'))->withPost($product);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        
        return view('products.edit',compact('product'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(productValidate $request, Product $product)
    {
        $validated = $request->validated();
       //$this->validateRequest($request->all());
        Storage::delete('public/'.$product->image);
       $product->name = $request->name;
       $product->detail = $request->detail;
       $file = $request->file;

       $fileName = $file->getClientOriginalName();
       //$file->move('uploads',$fileName);
       $file->storeAs('public', $fileName);
       $product->image = $fileName;
        $product->update();

        return redirect()->route('products.index')
        >with('success','Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        $product->delete();
        Storage::delete('public/'.$product->image);
        return redirect()->route('products.index')

                        ->with('success','Product deleted successfully');
    }

    /**
     *  validate
     */
    public function validateRequest ($request) {
        
        $messages = [
            'name.regex' => 'Name không đc chứa số.',
            'name.required' => 'Name chưa được nhập.',
            'file.image' => 'Định dạng ảnh không cho phép',
            'file.max' => 'Kích thước ảnh file quá lớn',
            'file.required' => 'chưa chọn ảnh',
            'detail.required' => 'detail chưa được nhập.'
            
        ];
        $validator = Validator::make($request,[

            'name' => 'required|max:150|min:100|regex:/^([^0-9]*)$/',
            'detail' => 'required',
            'file' => 'required|image|max:2028',

        ],$messages)->validate();

    }
}
