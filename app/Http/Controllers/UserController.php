<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;
class UserController extends Controller
{
    public function getLogin () {
        return view('home');
    }
    public function postLogin (Request $request)
    {
        if(Auth::attempt(['email'=> $request->Email , 'password'=> $request->Password])) {
            
            return redirect('/index');
        } else {
            return redirect('/userLogin');
        }

    }
}
