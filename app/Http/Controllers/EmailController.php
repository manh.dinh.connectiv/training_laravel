<?php
 
namespace App\Http\Controllers;
 
use Illuminate\Http\Request;
use Redirect,Response,DB,Config;
use Mail;
use App\Mail\MailNotify;
class EmailController extends Controller
{
    public function sendEmail(Request $request)
    {
        $email = $request->Email;
      Mail::to($email)->send(new MailNotify($email));
 
    
    }
}