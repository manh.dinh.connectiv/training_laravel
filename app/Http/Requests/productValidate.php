<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class productValidate extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function messages()
    {
        return [
            'name.regex' => 'Name không đc chứa số.',
            'name.required' => 'Name chưa được nhập.',
            'file.image' => 'Định dạng ảnh không cho phép',
            'file.max' => 'Kích thước ảnh file quá lớn',
            'file.required' => 'chưa chọn ảnh',
            'detail.required' => 'detail chưa được nhập.'
        ];
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:150|min:100|regex:/^([^0-9]*)$/',
            'detail' => 'required',
            'file' => 'required|image|max:2028',
        ];
    }
}
