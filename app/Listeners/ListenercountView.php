<?php

namespace App\Listeners;

use App\Events\countView;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class ListenercountView
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  countView  $event
     * @return void
     */
    public function handle(countView $event)
    {
        //
    }
}
