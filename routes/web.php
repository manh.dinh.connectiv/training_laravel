<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/',function (){
    return view('auth.login');
});
// Route::post('/laravel-send-email', 'EmailController@sendEMail')->name('sendMail');

// Route::get('/userLogin',function() {
//     return view('userLogin');
// });

// route::post('/postLogin', 'UserController@postLogin')->name('postLogin');


// Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

 Route::get('/verifyAccount','Auth\RegisterController@verifyAccount')->name('verifyAccount');

 Route::resource('products','ProductController');

 Route::middleware('throttle:5,1')->group(function () {
    Route::get('api/user', function () {
        //
    });
});