<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    //     $data = [
    //         'name' => 'dinh',
    //         'email' => 'dinh@gmail.com',
    //         'password' =>  bcrypt('123456')
    //     ];
    //    DB::table('tbl_user')->insert($data);
    //     // $this->call(UserSeeder::class);
    //$this->call(SupplierTableSeeder::class);
    $this->call(ProductTableSeeder::class);
    }
}
