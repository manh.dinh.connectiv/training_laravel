<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'detail'=>$faker->word,
        'image'=>$faker->image($dir = '/tmp', $width = 640, $height = 480),
        'created_at' => new DateTime,
        'updated_at' => new DateTime,
    ];
});
